import React from 'react'
import {render} from 'react-snapshot';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import toString from './toString'

export function run() {
	render(React.createElement(App), document.getElementById('root'));
}

export const renderToString = toString;

if (module.hot) {
	module.hot.accept('./App', () => {
		const NextApp = require('./App').default;
		render(React.createElement(NextApp), document.getElementById('root'))
	})
}

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
