import React from 'react'
import {Link, Route} from 'react-router-dom'
import styled from 'styled-components'

interface Props {
	title: string
	active: boolean
}

const NavLink = styled.div`
  a {
    transition: color 0.2s, border-bottom-color 0.2s;
    color: ${(props: Props) => props.active ? '#0000ff' : '#666'};
    text-decoration: none;
    border-bottom: 2px solid;
    border-bottom-color: ${(props: Props) => props.active ? 'rgba(0, 0, 255, 0.1)' : 'transparent'};
    &:hover, &:active, &:focus {
      color: ${(props: Props) => props.active ? '#0000ff' : '#222'};
    }
  }
`;

export interface RouteProps {
	title: string
	path: string
	component?: any
	exact?: boolean
}

export default (props: RouteProps) => {
	const {path, exact, title} = props;
	return (
		<Route path={path} exact={exact} children={({match}: any) => (
			<NavLink active={match}>
				<Link to={path}>{title}</Link>
			</NavLink>
		)}/>
	)
}
