const rewire = require('rewire');
const TerserPlugin = require('terser-webpack-plugin');
const defaults = rewire('react-scripts/scripts/build.js');
let config = defaults.__get__('config');

config.optimization.splitChunks = {
    cacheGroups: {
        default: false,
    },
};

config.optimization.minimize = false;
config.optimization.minimizer = [
    new TerserPlugin({
        terserOptions: {
            ecma: 6,
            mangle: false,
            module: true,
            keep_classnames: true,
            keep_fnames: true,
            output: {
                comments: false,
            },
        },
    }),
];
config.optimization.runtimeChunk = false;
config.output.filename = 'static/js/[name].js';
