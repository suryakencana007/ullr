/*  http.go
*
* @Author:             Nanang Suryadi <nanang.suryadi@kubuskotak.com>
* @Date:               September 30, 2018
* @Last Modified by:   @suryakencana007
* @Last Modified time: 30/09/18 01:37
 */

package cmd

import (
	"fmt"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"strings"
	"sync"
	"syscall"
	"time"

	"github.com/go-chi/chi"
	"github.com/olekukonko/tablewriter"
	"github.com/spf13/pflag"
	"gitlab.com/suryakencana007/ullr/configs"
	internalHttp "gitlab.com/suryakencana007/ullr/internal/http"

	"github.com/spf13/cobra"
	"github.com/suryakencana007/mimir/log"
)

func WelkomText() string {
	return `
========================================================================================  
                           888                        888      
                           888                        888      
                           888                        888      
.d8888b   8888b.  88888b.  88888b.   .d88b.   .d88b.  888  888 
88K          "88b 888 "88b 888 "88b d88""88b d88""88b 888 .88P 
"Y8888b. .d888888 888  888 888  888 888  888 888  888 888888K  
     X88 888  888 888  888 888  888 Y88..88P Y88..88P 888 "88b 
 88888P' "Y888888 888  888 888  888  "Y88P"   "Y88P"  888  888
========================================================================================
- port    : %d
- log     : %s
-----------------------------------------------------------------------------------------`
}

func ConfigPath() []string {
	return []string{
		"./configs",
		"../configs",
		"../../configs",
		"../../../configs"}
}

type HTTPCmd interface {
	serve(router http.Handler) error
	server(cmd *cobra.Command, args []string) (err error)
	GetBaseCmd() *cobra.Command
}

type httpCmd struct {
	stop <-chan bool

	BaseCmd       *cobra.Command
	configuration *configs.Config
	filename      string
	srv           *Server
}

// New Object HTTP Command
func NewHTTPCmd(
	configuration *configs.Config,
) HTTPCmd {
	return NewHTTPCmdSignaled(configuration, nil)
}

// New Object HTTP Command
func NewHTTPCmdSignaled(
	configuration *configs.Config,
	stop <-chan bool,
) HTTPCmd {
	cc := &httpCmd{stop: stop}
	cc.configuration = configuration
	cc.BaseCmd = &cobra.Command{
		Use:   "http",
		Short: "Used to run the http service",
		RunE:  cc.server,
	}
	fs := pflag.NewFlagSet("Root", pflag.ContinueOnError)
	fs.StringVarP(&cc.filename, "file", "f", "", "Custom configuration filename")
	cc.BaseCmd.Flags().AddFlagSet(fs)
	return cc
}

func (h *httpCmd) server(cmd *cobra.Command, args []string) (err error) {
	if len(h.filename) > 1 {
		h.configuration = configs.New(h.filename, ConfigPath()...)
	}

	router := internalHttp.GeneralMain(
		h.configuration,
	)
	// Description µ micro service
	fmt.Println(
		fmt.Sprintf(
			WelkomText(),
			h.configuration.App.Port,
			strings.Join([]string{
				h.configuration.Log.Dir,
				h.configuration.Log.Filename}, "/"),
		))
	tableRoute(router) // Prettier Route Pattern

	return h.serve(router)
}

func (h *httpCmd) serve(router http.Handler) error {
	addrURL := url.URL{Scheme: "http", Host: fmt.Sprintf("localhost:%d", h.configuration.App.Port)}
	log.Info(fmt.Sprintf("started server %s", addrURL.String()))
	h.srv = StartWebServer(
		addrURL,
		h.configuration.App.ReadTimeout,
		h.configuration.App.WriteTimeout,
		router,
	)
	defer h.srv.Stop()
	sc := make(chan os.Signal, 10)
	signal.Notify(sc, os.Interrupt, syscall.SIGTERM)
	select {
	case s := <-sc:
		log.Info(fmt.Sprintf("shutting down server with signal %q", s.String()))
	case <-h.stop:
		log.Info("shutting down server with stop channel")
	case <-h.srv.StopNotify():
		log.Info("shutting down server with stop signal")

	}
	return nil
}

func (h *httpCmd) GetBaseCmd() *cobra.Command {
	return h.BaseCmd
}

func tableRoute(r chi.Routes) {
	table := tablewriter.NewWriter(os.Stdout)
	table.SetHeader([]string{"Handler", "Url", "Method"})

	walkFunc := func(method string, route string, handler http.Handler, middleware ...func(http.Handler) http.Handler) error {
		table.Append([]string{"", route, method}) // Append Walk all routes
		return nil
	}
	_ = chi.Walk(r, walkFunc)
	table.Render() // Send output
}

// Server warps http.Server.
type Server struct {
	mu         sync.RWMutex
	addrURL    url.URL
	httpServer *http.Server

	stopc chan struct{}
	donec chan struct{}
}

// StartWebServer starts a web server
func StartWebServer(addr url.URL, readTimeout, writeTimeout int, handler http.Handler) *Server {
	stopc := make(chan struct{})
	srv := &Server{
		addrURL: addr,
		httpServer: &http.Server{
			Addr:         addr.Host,
			Handler:      handler,
			ReadTimeout:  time.Duration(readTimeout) * time.Second,
			WriteTimeout: time.Duration(writeTimeout) * time.Second,
		},
		stopc: stopc,
		donec: make(chan struct{}),
	}
	go func() {
		defer func() {
			if err := recover(); err != nil {
				log.Warn(
					"shutting down server with err ",
					log.Field("error", fmt.Sprintf(`(%v)`, err)),
				)
				os.Exit(0)
			}
			close(srv.donec)
		}()
		if err := srv.httpServer.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatal(
				"shutting down server with err ",
				log.Field("error", err),
			)
		}
	}()
	return srv
}

// StopNotify returns receive-only stop channel to notify the server has stopped.
func (srv *Server) StopNotify() <-chan struct{} {
	return srv.stopc
}

// Stop stops the server. Useful for testing.
func (srv *Server) Stop() {
	log.Warn(fmt.Sprintf("stopping server %s", srv.addrURL.String()))
	srv.mu.Lock()
	if srv.httpServer == nil {
		srv.mu.Unlock()
		return
	}
	close(srv.stopc)
	_ = srv.httpServer.Close()
	<-srv.donec
	srv.mu.Unlock()
	log.Warn(fmt.Sprintf("stopped server %s", srv.addrURL.String()))
}
