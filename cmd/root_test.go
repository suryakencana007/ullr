/*  main_test.go
*
* @Author:             Nanang Suryadi <nanang.suryadi@kubuskotak.com>
* @Date:               October 18, 2018
* @Last Modified by:   @suryakencana007
* @Last Modified time: 18/10/18 02:16
 */

package cmd

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAppMain(t *testing.T) {
	os.Args = []string{"main", "-f", "configs/app.config.test.toml"}
	Execute()
	os.Args = []string{""}
}

func TestFailAppMain(t *testing.T) {
	os.Args = []string{"main", "htt"}
	f := func() {
		Execute()
	}

	assert.Panics(t, f)

	os.Args = []string{""}
}
