/*  http_test.go
*
* @Author:             Nanang Suryadi <nanang.suryadi@kubuskotak.com>
* @Date:               October 15, 2018
* @Last Modified by:   @suryakencana007
* @Last Modified time: 15/10/18 03:38
 */

package cmd

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"sync"
	"testing"

	"github.com/go-chi/chi"
	"github.com/spf13/cobra"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/suryakencana007/ullr/configs"
	internalHttp "gitlab.com/suryakencana007/ullr/internal/http"
)

func table() string {
	return `+---------+---------------------------+--------+
| HANDLER |            URL            | METHOD |
+---------+---------------------------+--------+
|         | /v1/*/api/status/*/health | GET    |
+---------+---------------------------+--------+
`
}

func TestTableRoute(t *testing.T) {
	configuration := configs.New(fmt.Sprintf(`app.config.%s`, "test"), ConfigPath()...)
	router := internalHttp.GeneralMain(configuration)

	oldStdout := os.Stdout
	r, w, _ := os.Pipe()

	os.Stdout = w
	outC := make(chan string)
	tableRoute(router)
	go func() {
		var buf bytes.Buffer
		if _, err := io.Copy(&buf, r); err == nil {
			outC <- buf.String()
		}
	}()
	// Reset the output again
	w.Close()
	os.Stdout = oldStdout
	out := <-outC
	assert.Equal(t, table(), out)
}

func TestNewHttp(t *testing.T) {
	var wg sync.WaitGroup
	assert := require.New(t)
	stop := make(chan bool)
	wg.Add(1)
	configuration := configs.New("app.config.test",
		"./configs", "../configs", "../../../configs")

	http := NewHTTPCmdSignaled(configuration, stop)

	go func() {
		defer wg.Done()
		err := http.GetBaseCmd().Execute()
		assert.NoError(err)
	}()

	stop <- true
	wg.Wait()
}

func TestHttp(t *testing.T) {
	var wg sync.WaitGroup
	assert := require.New(t)
	stop := make(chan bool)
	wg.Add(1)
	configuration := configs.New("app.config.test",
		"./configs", "../configs", "../../../configs")

	cmd := NewHTTPCmdSignaled(configuration, stop).GetBaseCmd()
	go func() {
		defer wg.Done()
		_, err := cmd.ExecuteC()
		assert.NoError(err)
	}()

	stop <- true
	wg.Wait()
}

func TestHttpFail(t *testing.T) {
	var (
		wg            sync.WaitGroup
		err           error
		stop          = make(chan bool)
		configuration *configs.Config
	)

	wg.Add(1)
	f := func() {
		configuration = configs.New("app.config",
			"./configs", "../configs", "../../configs")
	}
	assert.Panics(t, f)
	cmd := NewHTTPCmdSignaled(configuration, stop).GetBaseCmd()

	go func() {
		defer wg.Done()
		fn := func() {
			_, err = cmd.ExecuteC()
		}
		assert.Panics(t, fn)
		<-stop
	}()

	assert.NoError(t, err)
	stop <- true
	wg.Wait()
}

func TestNewHttpCmdWithFilename(t *testing.T) {
	var wg sync.WaitGroup
	stop := make(chan bool)
	configuration := configs.New("app.config.test", ConfigPath()...)

	wg.Add(1)
	os.Args = []string{"main", "http", "-f", "app.config.test"}

	cmd := NewHTTPCmdSignaled(configuration, stop).GetBaseCmd()
	go func() {
		defer wg.Done()

		_, err := cmd.ExecuteC()
		assert.NoError(t, err)
	}()

	stop <- true
	wg.Wait()
	os.Args = []string{""}
}

func TestListenAndServe(t *testing.T) {
	var (
		err error
		wg  sync.WaitGroup
	)
	stop := make(chan bool)

	configuration := configs.New("app.config.test", ConfigPath()...)

	cc := &httpCmd{stop: stop}
	cc.configuration = configuration
	cc.BaseCmd = &cobra.Command{
		Use:   "http",
		Short: "Used to run the http service",
		RunE: func(cmd *cobra.Command, args []string) (err error) {
			mux := chi.NewMux()
			return cc.serve(mux)
		},
	}

	wg.Add(1)
	go func() {
		defer wg.Done()
		err = cc.BaseCmd.Execute()
	}()
	assert.NoError(t, err)
	stop <- true
	wg.Wait()
}
