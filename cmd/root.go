/*  root.go
*
* @Author:             Nanang Suryadi
* @Date:               April 11, 2019
* @Last Modified by:   @suryakencana007
* @Last Modified time: 2019-04-11 23:02
 */

package cmd

import (
	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
	"gitlab.com/suryakencana007/ullr/configs"
)

// NewRoot returns a newly initialized cobra.Command object that implements the Command
// interface.
func NewRoot() *cobra.Command {
	return &cobra.Command{
		Use:   "sanhook",
		Short: "sanhook - Stock::Inventory µService",
		Long:  "sanhook is Micro Service for Inventory Management",
	}
}

func Execute() {
	var filename string
	fs := pflag.NewFlagSet("Root", pflag.ContinueOnError)
	fs.StringVarP(&filename,
		"file",
		"f",
		"app.config.test",
		"Custom configuration filename",
	)
	root := NewRoot()
	root.Flags().AddFlagSet(fs)
	configuration := configs.New(filename, ConfigPath()...)
	root.AddCommand(
		NewHTTPCmd(
			configuration,
		).GetBaseCmd(),
	)
	if err := root.Execute(); err != nil {
		panic(err)
	}
}
