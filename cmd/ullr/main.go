/*  main.go
*
* @Author:             Nanang Suryadi
* @Date:               April 11, 2019
* @Last Modified by:   @suryakencana007
* @Last Modified time: 2019-04-11 00:40
 */

package main

import (
	"gitlab.com/suryakencana007/ullr/cmd"
)

func main() {
	cmd.Execute()
}
