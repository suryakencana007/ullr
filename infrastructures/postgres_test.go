// +build integration

/*  postgres_test.go
*
* @Author:             Nanang Suryadi
* @Date:               February 13, 2019
* @Last Modified by:   @suryakencana007
* @Last Modified time: 2019-02-13 10:34
 */

package infrastructures

import (
	"testing"

	"github.com/suryakencana007/mimir/sql"
	"gitlab.com/suryakencana007/ullr/configs"
)

func openTestConnPG(t *testing.T, connInfo string, c *configs.Config) sql.DBFactory {
	db := NewPgSQL()
	db.OpenConnection(connInfo,
		c.CB.Retry,
		c.CB.Timeout,
		c.CB.Concurrent,
	)
	d, _ := db.GetDB()
	t.Log(getServerVersion(t, d))
	return db
}

func getServerVersion(t *testing.T, db *sql.DB) int {
	var version int
	row, _ := db.QueryRow("SHOW server_version_num")
	err := row.Scan(&version)
	if err != nil {
		t.Fatal(err)
	}
	return version
}
