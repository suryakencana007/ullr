/*  postgres.go
*
* @Author:             Nanang Suryadi
* @Date:               February 13, 2019
* @Last Modified by:   @suryakencana007
* @Last Modified time: 2019-02-13 10:32
 */

package infrastructures

import (
	_ "github.com/lib/pq" // interface pq returns a Postgres SQL API
	"github.com/suryakencana007/mimir/constant"
	"github.com/suryakencana007/mimir/log"
	"github.com/suryakencana007/mimir/sql"
)

// PgInfrastructure returns a newly initialized sql.DB struct that implements the sql.DBFactory
// interface.
type PgInfrastructure struct {
	*sql.DB
}

// NewPgSQL returns a newly initialized PgInfrastructure struct that implements the sql.DBFactory
// interface.
func NewPgSQL() sql.DBFactory {
	return &PgInfrastructure{}
}

// OpenConnection gets a handle for a database
func (s *PgInfrastructure) OpenConnection(connString string, retry, timeout, concurrent int) {

	db, err := sql.Open(
		constant.POSTGRES,
		connString,
		retry,
		timeout,
		concurrent,
	)
	if err != nil {
		panic(err.Error())
	}
	s.DB = db
	_, err = s.GetDB()
	if err != nil {
		panic(err.Error())
	}
}

// GetDB gets database connection
func (s *PgInfrastructure) GetDB() (*sql.DB, error) {
	err := s.Ping()
	if err != nil {
		log.Error("Get DB",
			log.Field("error", err.Error()),
		)
		return nil, err
	}

	return s.DB, nil
}
