// +build integration

/*  postgres_main_test.go
*
* @Author:             Nanang Suryadi
* @Date:               February 19, 2019
* @Last Modified by:   @suryakencana007
* @Last Modified time: 2019-02-19 17:05
 */

package infrastructures

import (
	"fmt"
	"testing"

	"github.com/lib/pq"
	"github.com/stretchr/testify/assert"
	"github.com/suryakencana007/mimir/constant"
	"github.com/suryakencana007/mimir/sql"
	"gitlab.com/suryakencana007/ullr/configs"
)

func setupSourceMain() (string, *configs.Config) {
	c := configs.New("app.config.dev",
		"./configs", "../configs", "../../configs")
	return fmt.Sprintf(constant.POSTGRES_CONN, c.PgOdoo.Host, c.PgOdoo.Port, c.PgOdoo.User, c.PgOdoo.Password, c.PgOdoo.DbName), c
}

func setupSourceMainFail(port int, dbname string) (string, *configs.Config) {
	c := configs.New("app.config.dev",
		"./configs", "../configs", "../../configs")
	return fmt.Sprintf(constant.POSTGRES_CONN, c.Postgres.Host, port, c.Postgres.User, c.Postgres.Password, dbname), c
}

func TestMainGetDB(t *testing.T) {
	conn, c := setupSourceMain()
	db := openTestConnPG(t, conn, c)
	defer db.Close()
	var sqlDb *sql.DB
	sqlDb, err := db.GetDB()
	assert.IsType(t, sqlDb, sqlDb)
	if err != nil {
		panic(fmt.Errorf("failed dial database : %v", err))
	}
}

func TestMainGetDBFail(t *testing.T) {
	connInfo, c := setupSourceMainFail(5438, "db")
	f := func() {
		db := openTestConnPG(t, connInfo, c)
		defer db.Close()
		dbType, _ := db.GetDB()
		assert.Nil(t, dbType)
	}
	assert.Panics(t, f)
	assert.PanicsWithValue(t, fmt.Sprintf("dial tcp %s:5438: connect: connection refused", c.Postgres.Host), f)
}

func TestMainReconnect(t *testing.T) {
	conn, c := setupSourceMain()
	db1 := openTestConnPG(t, conn, c)
	defer db1.Close()
	tx, err := db1.Begin()
	if err != nil {
		t.Fatal(err)
	}
	var pid1 int
	err = tx.QueryRow("SELECT pg_backend_pid()").Scan(&pid1)
	if err != nil {
		t.Fatal(err)
	}

	db2 := openTestConnPG(t, conn, c)
	defer db2.Close()
	_, err = db2.Exec("SELECT pg_terminate_backend($1)", pid1)
	if err != nil {
		t.Fatal(err)
	}
	// The rollback will probably "fail" because we just killed
	// its connection above
	_ = tx.Rollback()

	const expected int = 42
	var result int
	row, _ := db1.QueryRow(fmt.Sprintf("SELECT %d", expected))
	err = row.Scan(&result)
	if err != nil {
		t.Fatal(err)
	}
	if result != expected {
		t.Errorf("got %v; expected %v", result, expected)
	}
}

func TestMainCommitInFailedTransaction(t *testing.T) {
	conn, c := setupSourceMain()
	db := openTestConnPG(t, conn, c)
	defer db.Close()

	txn, err := db.Begin()
	if err != nil {
		t.Fatal(err)
	}
	rows, err := txn.Query("SELECT error")
	if err == nil {
		rows.Close()
		t.Fatal("expected failure")
	}
	err = txn.Commit()
	if err != pq.ErrInFailedTransaction {
		t.Fatalf("expected ErrInFailedTransaction; got %#v", err)
	}
}

// schema table does not existing
func TestGetDBSchema(t *testing.T) {
	conn, c := setupSourceMain()
	db := openTestConnPG(t, conn, c)
	defer db.Close()

	tx, err := db.Begin()
	if err != nil {
		t.Fatal(err)
	}

	rows, err := tx.Query("SELECT login FROM res_users")
	if err != nil {
		t.Fatal(err)
	}

	defer rows.Close()
	names := make([]string, 0)
	for rows.Next() {
		var name string
		if err := rows.Scan(&name); err != nil {
			t.Fatal(err)
		}
		names = append(names, name)
	}
	t.Log(names)
	if err := rows.Err(); err != nil {
		t.Fatal(err)
	}
	_ = tx.Commit()
}
