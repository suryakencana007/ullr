/*  http_test.go
*
* @Author:             Nanang Suryadi
* @Date:               February 21, 2019
* @Last Modified by:   @suryakencana007
* @Last Modified time: 2019-02-21 15:36
 */

package http

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/suryakencana007/mimir/response"
	"gitlab.com/suryakencana007/ullr/configs"
)

func TestHTTPTransport(t *testing.T) {
	// create instance mux route
	c := configs.New("app.config.test",
		"./configs", "../configs", "../../configs")
	r := Middleware(c)
	r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		body := response.NewAWSResponse()
		body.SetData(map[string]interface{}{
			"Status": "Health is OK",
		})
		response.WriteJSON(w, r, body)
	})
	ts := httptest.NewServer(r)
	defer ts.Close()

	// check that we didn't break correct routes
	// Route: /v1/api/status/health
	res, health := testRequest(t, ts, "/")
	if got, want := res.StatusCode, http.StatusOK; got != want {
		t.Fatalf("status code got: %d, want %d", got, want)
	}

	actual := response.AWSResponse{}
	err := json.Unmarshal([]byte(health), &actual)
	if err != nil {
		t.Fatal(err)
	}
	expected := response.NewAWSResponse()
	expected.SetData(map[string]interface{}{
		"Status": "Health is OK",
	})
	assert.Equal(t, expected, actual)
	// :GET /v1/api/status/health
}

func testRequest(t *testing.T, ts *httptest.Server, path string) (*http.Response, string) {
	req, err := http.NewRequest("GET", ts.URL+path, nil)
	if err != nil {
		t.Fatal(err)
		return nil, ""
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatal(err)
		return nil, ""
	}

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatal(err)
		return nil, ""
	}
	defer resp.Body.Close()

	return resp, string(respBody)
}
