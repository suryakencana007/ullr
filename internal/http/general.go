/*  general.go
*
* @Author:             Nanang Suryadi
* @Date:               March 10, 2019
* @Last Modified by:   @suryakencana007
* @Last Modified time: 2019-03-10 11:37
 */

package http

import (
	"fmt"
	"net/http"

	assetfs "github.com/elazarl/go-bindata-assetfs"
	"github.com/go-chi/chi"
	"gitlab.com/suryakencana007/ullr/configs"
	"gitlab.com/suryakencana007/ullr/frontend"
)

// GeneralMain returns a newly initialized chi.Mux object that implements the Router
// interface.
func GeneralMain(c *configs.Config) *chi.Mux {
	r := Middleware(c)

	afs := assetfs.AssetFS{
		Asset:     frontend.Asset,
		AssetDir:  frontend.AssetDir,
		AssetInfo: frontend.AssetInfo,
		Prefix:    "",
	}
	fileServerHandler := http.FileServer(&afs)
	fe := &Frontend{fileServer: fileServerHandler}
	r.Use(fe.Handler)
	// Rest API ver. 1
	r.Route("/v1", func(r chi.Router) {
		r.Mount(
			fmt.Sprintf(`%s/%s`, c.API.Prefix, "status"),
			HealthTransport(c),
		)
	})
	return r
}
