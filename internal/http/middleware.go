/*  http.go
*
* @Author:             Nanang Suryadi
* @Date:               February 20, 2019
* @Last Modified by:   @suryakencana007
* @Last Modified time: 2019-02-20 18:35
 */

package http

import (
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/cors"
	"gitlab.com/suryakencana007/ullr/configs"
)

// Middleware returns a newly initialized chi.Mux object that implements the Router
// interface.
func Middleware(configuration *configs.Config) *chi.Mux {
	router := chi.NewRouter()

	router.Use(
		middleware.RedirectSlashes,
		middleware.Recoverer,
		middleware.StripSlashes,
	)
	cors := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: true,
		MaxAge:           300,
	})
	router.Use(cors.Handler)
	return router
}
