/*  health_test.go
*
* @Author:             Nanang Suryadi
* @Date:               February 21, 2019
* @Last Modified by:   @suryakencana007
* @Last Modified time: 2019-02-21 01:37
 */

package http

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-chi/chi"
	"github.com/stretchr/testify/assert"
	"github.com/suryakencana007/mimir/response"
	"gitlab.com/suryakencana007/ullr/configs"
)

func TestTransport(t *testing.T) {
	// create instance mux route
	c := configs.New("app.config.test",
		"./configs", "../configs", "../../configs")
	r := chi.NewRouter()
	// API ver. 1
	r.Route("/v1", func(route chi.Router) {
		route.Mount(fmt.Sprintf(`%s/%s`, c.API.Prefix, "status"), HealthTransport(c))
	})

	ts := httptest.NewServer(r)
	defer ts.Close()

	// check that we didn't break correct routes
	// Route: /v1/api/status/health
	res, b := testRequest(t, ts, "/v1/api/status/health")
	if got, want := res.StatusCode, http.StatusOK; got != want {
		t.Fatalf("status code got: %d, want %d", got, want)
	}
	actual := response.AWSResponse{}
	err := json.Unmarshal([]byte(b), &actual)
	if err != nil {
		t.Fatal(err)
	}
	expected := response.NewAWSResponse()
	expected.SetData(map[string]interface{}{
		"Status": "Health is OK",
	})
	assert.Equal(t, expected, actual)
	// :GET /v1/api/status/health
}
