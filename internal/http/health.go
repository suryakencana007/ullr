/*  health.go
*
* @Author:             Nanang Suryadi
* @Date:               February 20, 2019
* @Last Modified by:   @suryakencana007
* @Last Modified time: 2019-02-20 18:37
 */

package http

import (
	"github.com/go-chi/chi"
	"gitlab.com/suryakencana007/ullr/configs"
	"gitlab.com/suryakencana007/ullr/internal/health"
)

// HealthTransport returns a newly initialized chi.Mux object that implements the Router
// interface.
func HealthTransport(c *configs.Config) *chi.Mux {
	r := chi.NewRouter()
	r.Get("/health", health.GetAPIStatus())
	return r
}
