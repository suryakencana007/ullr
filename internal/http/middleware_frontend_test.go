/*  middleware_frontend_test.go
*
* @Author:             Nanang Suryadi
* @Date:               April 19, 2019
* @Last Modified by:   @suryakencana007
* @Last Modified time: 2019-04-19 23:28
 */

package http

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	assetfs "github.com/elazarl/go-bindata-assetfs"
	"github.com/stretchr/testify/assert"
	"github.com/suryakencana007/mimir/response"
	"gitlab.com/suryakencana007/ullr/configs"
	"gitlab.com/suryakencana007/ullr/frontend"
)

func TestMiddlewareTransport(t *testing.T) {
	// create instance mux route
	c := configs.New("app.config.test",
		"./configs", "../configs", "../../configs")
	r := Middleware(c)
	afs := assetfs.AssetFS{
		Asset:     frontend.Asset,
		AssetDir:  frontend.AssetDir,
		AssetInfo: frontend.AssetInfo,
		Prefix:    "",
	}
	fileServerHandler := http.FileServer(&afs)
	fe := &Frontend{fileServer: fileServerHandler}
	r.Use(fe.Handler)
	r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		body := response.NewAWSResponse()
		body.SetData(map[string]interface{}{
			"Status": "Health is OK",
		})
		response.WriteJSON(w, r, body)
	})
	ts := httptest.NewServer(r)
	defer ts.Close()

	// check that we didn't break correct routes
	// Route: /v1/api/status/health
	res, health := testRequest(t, ts, "/")
	if got, want := res.StatusCode, http.StatusOK; got != want {
		t.Fatalf("status code got: %d, want %d", got, want)
	}

	actual := response.AWSResponse{}
	err := json.Unmarshal([]byte(health), &actual)
	if err != nil {
		t.Fatal(err)
	}
	expected := response.NewAWSResponse()
	expected.SetData(map[string]interface{}{
		"Status": "Health is OK",
	})
	assert.Equal(t, expected, actual)

	resAbout, about := testRequest(t, ts, "/about")
	if got, want := resAbout.StatusCode, http.StatusOK; got != want {
		t.Fatalf("status code got: %d, want %d", got, want)
	}

	// :GET /about
	t.Log(about)

	resManifest, manifest := testRequest(t, ts, "/manifest.json")
	if got, want := resManifest.StatusCode, http.StatusOK; got != want {
		t.Fatalf("status code got: %d, want %d", got, want)
	}

	// :GET /manifest.json
	actualManifest := map[string]interface{}{}
	err = json.Unmarshal([]byte(manifest), &actualManifest)
	if err != nil {
		t.Fatal(err)
	}

	assert.Contains(t, actualManifest, "short_name")
	assert.Contains(t, actualManifest, "name")
	assert.Contains(t, actualManifest, "display")
}
