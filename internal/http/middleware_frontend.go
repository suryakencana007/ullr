/*  middleware_frontend.go
*
* @Author:             Nanang Suryadi
* @Date:               April 15, 2019
* @Last Modified by:   @suryakencana007
* @Last Modified time: 2019-04-15 02:48
 */

package http

import (
	"fmt"
	"net/http"
	"regexp"

	"github.com/go-chi/chi"
	"gitlab.com/suryakencana007/ullr/frontend"
)

type Frontend struct {
	fileServer http.Handler
}

func (f *Frontend) Handler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		rctx := chi.RouteContext(r.Context())
		tctx := chi.NewRouteContext()
		if rctx.Routes.Match(tctx, r.Method, r.URL.Path) {
			next.ServeHTTP(w, r)
			return
		}

		re := regexp.MustCompile(`\.[a-zA-Z0-9]+`)

		if ok := re.Match([]byte(r.URL.Path[1:])); !ok {
			pathAsset := fmt.Sprintf(`%s%s`, r.URL.Path[1:], ".html")
			if v, err := frontend.Asset(pathAsset); err == nil {
				if _, err := w.Write(v); err == nil {
					return
				}
			}
		}

		f.fileServer.ServeHTTP(w, r)
	})
}
