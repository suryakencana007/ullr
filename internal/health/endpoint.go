/*  endpoint.go
*
* @Author:             Nanang Suryadi
* @Date:               February 20, 2019
* @Last Modified by:   @suryakencana007
* @Last Modified time: 2019-02-20 18:42
 */

package health

import (
	"net/http"

	"github.com/suryakencana007/mimir/response"
)

// GetStatus gets the service health status
func GetAPIStatus() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		body := response.NewAWSResponse()
		body.SetData(map[string]interface{}{
			"Status": "Health is OK",
		})
		response.WriteJSON(w, r, body)
	}
}
