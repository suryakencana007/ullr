/*  endpoint_test.go
*
* @Author:             Nanang Suryadi
* @Date:               February 21, 2019
* @Last Modified by:   @suryakencana007
* @Last Modified time: 2019-02-21 01:20
 */

package health

import (
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/suryakencana007/mimir/request"
)

// Unit Test Health
func TestHealth(t *testing.T) {
	res, _ := testHandler(t,
		GetAPIStatus(),
		http.MethodGet, "/health", nil)
	if got, want := res.StatusCode, http.StatusOK; got != want {
		t.Fatalf("status code got: %d, want %d", got, want)
	}
}

func testHandler(
	t *testing.T,
	h http.Handler,
	method, path string,
	body io.Reader,
) (*http.Response, string) {
	bodyReader, err := request.BodyToJson(body)
	if err != nil {
		panic(err)
	}
	r, _ := http.NewRequest(method, path, bodyReader)
	w := httptest.NewRecorder()
	h.ServeHTTP(w, r)
	t.Log(body)
	return w.Result(), w.Body.String()
}
