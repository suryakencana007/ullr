/*  main_db_pg.go
*
* @Author:             Nanang Suryadi
* @Date:               March 10, 2019
* @Last Modified by:   @suryakencana007
* @Last Modified time: 2019-03-10 22:22
 */

package container

import (
	"fmt"

	"github.com/suryakencana007/mimir/constant"
	"gitlab.com/suryakencana007/ullr/infrastructures"
)

// BuildMainDB returns bounded context of Container object
// void.
func (c *Container) BuildMainDB() {
	dbRoute := infrastructures.NewPgSQL()
	dbConn := fmt.Sprintf(
		constant.POSTGRES_CONN,
		c.Postgres.Host,
		c.Postgres.Port,
		c.Postgres.User,
		c.Postgres.Password,
		c.Postgres.DbName,
	)
	dbRoute.OpenConnection(
		dbConn,
		c.CB.Retry,
		c.CB.Timeout,
		c.CB.Concurrent,
	)
	c.DB = dbRoute
}
