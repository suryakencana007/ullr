/*  container.go
*
* @Author:             Nanang Suryadi
* @Date:               February 19, 2019
* @Last Modified by:   @suryakencana007
* @Last Modified time: 2019-02-19 16:37
 */

package container

import (
	"sync"

	"github.com/suryakencana007/mimir/sql"
	"gitlab.com/suryakencana007/ullr/configs"
)

type Factory interface {
	Build() *Container
	// BuildOdoo()
	// BuildMainDB()
	// NewSaleOrderService() order.SaleOrderService
	// NewWarehouseService() inventory.WarehouseService
	// NewRouteScheduleService() order.RouteScheduleService
	// NewRouteFleetService() order.RouteFleetService
}

type Container struct {
	*configs.Config
	DBWpd sql.DBFactory
	DB    sql.DBFactory
}

var (
	once      sync.Once
	container *Container
)

func Instance(config *configs.Config) Factory {
	once.Do(func() {
		container = &Container{
			Config: config,
		}
	})
	return container
}

func (c *Container) Build() *Container {
	return c
}
