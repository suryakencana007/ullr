module gitlab.com/suryakencana007/ullr

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/elazarl/go-bindata-assetfs v1.0.0
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/go-chi/cors v1.0.0
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/lib/pq v1.0.0
	github.com/mattn/go-runewidth v0.0.4 // indirect
	github.com/olekukonko/tablewriter v0.0.1
	github.com/onsi/ginkgo v1.8.0 // indirect
	github.com/onsi/gomega v1.5.0 // indirect
	github.com/pkg/errors v0.8.1 // indirect
	github.com/satori/go.uuid v1.2.0
	github.com/smartystreets/goconvey v0.0.0-20190330032615-68dc04aab96a // indirect
	github.com/spf13/cobra v0.0.3
	github.com/spf13/pflag v1.0.3
	github.com/spf13/viper v1.3.2
	github.com/stretchr/testify v1.3.0
	github.com/suryakencana007/mimir v1.1.4
	golang.org/x/net v0.0.0-20190415100556-4a65cf94b679 // indirect
)
