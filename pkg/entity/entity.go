/*  entity.go
*
* @Author:             Nanang Suryadi
* @Date:               February 12, 2019
* @Last Modified by:   @suryakencana007
* @Last Modified time: 2019-02-12 18:11
 */

package entity

import (
	"time"

	"github.com/satori/go.uuid"
	"github.com/suryakencana007/mimir/generator"
)

// GenID returns a newly initialized string object that implements the RandomChar
// interface.
func GenID() string {
	return generator.RandomChar(10)
}

// UUID returns a newly initialized string object that implements the UUID
// interface.
func UUID() string {
	return uuid.NewV4().String()
}

// Event return struct.
type Event struct {
	CreatedAt time.Time `json:"created_at,omitempty"`
	CreatedBy string    `json:"created_by,omitempty"`
	UpdatedAt time.Time `json:"updated_at,omitempty"`
	UpdatedBy string    `json:"updated_by,omitempty"`
	DeletedAt time.Time `json:"deleted_at,omitempty"`
}
