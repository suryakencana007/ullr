/*  warehouse.go
*
* @Author:             Nanang Suryadi
* @Date:               February 22, 2019
* @Last Modified by:   @suryakencana007
* @Last Modified time: 2019-02-22 13:13
 */

package entity

import (
	"github.com/suryakencana007/mimir/sql"
)

type RepositoryWarehouse interface {
	Find(code int) (Warehouse, error)
	FindAll(p *sql.Pagination) ([]Warehouse, error)
}

type Warehouse struct {
	ID             int            `json:"id"`
	Code           sql.NullString `json:"code"`
	Name           sql.NullString `json:"name"`
	IsActive       sql.NullBool   `json:"is_active"`
	CompanyID      sql.NullInt64  `json:"company_id"`
	ReceptionSteps sql.NullString `json:"reception_steps"`
	DeliverySteps  sql.NullString `json:"delivery_steps"`
}

func (Warehouse) TableName() string {
	return "stock_warehouse"
}
