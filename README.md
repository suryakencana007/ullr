## Ullr (WPD Delivery Routing) service
Ullr is React Go! Project

[![Build Status](https://gitlab.com/suryakencana007/ullr/badges/master/build.svg)](https://gitlab.com/suryakencana007/ullr/commits/master) [![Coverage Report](https://gitlab.com/suryakencana007/ullr/badges/master/coverage.svg)](https://gitlab.com/suryakencana007/ullr/commits/master) [![Go Report Card](https://goreportcard.com/badge/gitlab.com/suryakencana007/ullr)](https://goreportcard.com/report/gitlab.com/suryakencana007/ullr) [![License MIT](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://img.shields.io/badge/License-MIT-brightgreen.svg)

### Pre-requirement
* Go 1.11+
* Python3
* Brew (OSX)
* Node 8+

### Structure Project
```md
+--
|-- bin # binary for deploy on aws lambda
|-- cmd # main handler
|-- configs # configuration file and setup
    |-- app.config.toml # config properties 
|-- infrastructures # provider setup i.e postgres, mariadb, redis dkk
|-- internal # services and repository local package
|-- pkg # common package for external use
|-- go.mod # go11+ module package management
|-- gomod.sh # initial go module
|-- Makefile # makefile cli to build and deploy
|-- README.md
|-- serverless.yml # configuration file for aws lambda 
|-- tempalte.yml # configuration file for sam aws local
```
