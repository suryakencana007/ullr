PROJECT_NAME := "ullr"
PKG := "gitlab.com/suryakencana007/$(PROJECT_NAME)"
PKG_LIST := $(shell go list ${PKG}/... | grep -v /vendor/)

BIN           = $(GOPATH)/bin
GO_BINDATA    = $(BIN)/go-bindata
BINDATA       = frontend/bindata.go
BINDATA_FLAGS = -pkg=frontend -prefix=frontend/build
PID           = .pid


.PHONY: all build dep lint coverage coverhtml race clean deploy gomodgen frontend serve

all: build

lint: ## Lint the files
	@golangci-lint run ${${PKG}/...}

test: ## Run unittests
	@go test -short ${PKG_LIST}

kill:
	@kill `cat $(PID)` || true

build: gomodgen
	export GO111MODULE=on
	go build -ldflags="-s -w" -o bin/ullr cmd/ullr/main.go
	# env GOOS=linux go build -ldflags="-s -w" -o bin/ullr cmd/ullr/main.go

clean:
	rm -rf ./bin ./vendor Gopkg.lock ./frontend/build ./frontend/bindata.go

frontend:
	cd ./frontend && yarn && yarn build
	cd ..
	$(GO_BINDATA) $(BINDATA_FLAGS) -o=$(BINDATA) frontend/build/...

deploy: clean gomodgen build
	sls deploy --verbose

gomodgen:
	./tools/gomod.sh;

dep:
	@go mod download

serve:
	go run cmd/app/main.go --dev & echo $$! > $(PID)
	cd ./frontend && yarn start

race: ## Run data race detector
	@go test -race ${PKG_LIST}

coverage: ## Generate global code coverage report
	./tools/coverage.sh;

coverhtml: ## Generate global code coverage report in HTML
	./tools/coverage.sh html;
