#!/bin/sh
set -eu

touch go.mod

PROJECT_NAME=suryakencana007
CURRENT_DIR=$(basename $(pwd))

CONTENT=$(cat <<-EOD
module gitlab.com/${PROJECT_NAME}/${CURRENT_DIR}

require github.com/elazarl/go-bindata-assetfs v1.0.0
EOD
)

echo "$CONTENT" > go.mod
