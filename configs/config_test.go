/*  config_test.go
*
* @Author:             Nanang Suryadi
* @Date:               October 10, 2018
* @Last Modified by:   @suryakencana007
* @Last Modified time: 10/10/18 00:34
 */

package configs

import (
	"bytes"
	"testing"

	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
)

func config() []byte {
	return []byte(`
[App] # App configuration
name = "µs-bifrost"
port = 8080
read_timeout = 5 # seconds
write_timeout = 10 # seconds
timezone = "Asia/Jakarta"
debug = false # (true|false)
env = "development" # ('development'|'staging'|'production')
secret_key = ""

[API] # Rest API configuration
prefix = "/api"

[Log] # Log configuration
dir = "logs"
filename = "bifrost.log"

[CB]
retry_count = 3
db_timeout = 500
max_concurrent = 5

[Postgres]
db_name = "ullr"
host = "172.31.16.246"
port = 5430
user = "project"
password = "sekret"
max_life_time = 30
max_idle_connection = 0
max_open_connection = 0

[PgOdoo]
db_name = "wpddev"
host = "127.0.1"
port = 5438
user = "user"
password = "sekret"
max_life_time = 30
max_idle_connection = 0
max_open_connection = 0
`)
}

func configNoLog() []byte {
	return []byte(`
[App] # App configuration
name = "µs-wyonna"
port = 8080
read_timeout = 5 # seconds
write_timeout = 10 # seconds
timezone = "Asia/Jakarta"
debug = false # (true|false)
env = "development" # ('development'|'staging'|'production')
secret_key = ""

[API] # Rest API configuration
prefix = "/api"`)
}

func initConfig(t *testing.T, c []byte) {
	viper.Reset()
	viper.New()
	viper.SetConfigType("toml")
	r := bytes.NewReader(c)

	var (
		err error
		n   int64
	)
	buf := new(bytes.Buffer)
	n, err = buf.ReadFrom(r)

	assert.IsType(t, int64(345), n)
	assert.Nil(t, err)
	err = viper.ReadConfig(buf)
	assert.Nil(t, err)
}

func TestNew(t *testing.T) {
	initConfig(t, config())
	var constants Constants
	err := viper.Unmarshal(&constants)
	assert.Nil(t, err)

	c := Config{}
	c.Constants = constants
	configuration := New("app.config.test",
		"./configs", "../configs", "../../configs")
	assert.Equal(t, &c, configuration)

}

func TestNewNoLog(t *testing.T) {
	initConfig(t, configNoLog())
	var constants Constants
	err := viper.Unmarshal(&constants)
	assert.Nil(t, err)

	c := Config{}
	c.Constants = constants
	configuration := New("app.config.test",
		"./configs", "../configs", "../../configs")
	assert.NotEqual(t, &c, configuration)
}

func TestNewFailTOML(t *testing.T) {
	f := func() {
		New("app.config.fail",
			"./configs", "../configs", "../../configs")
	}
	assert.Panics(t, f)
	assert.PanicsWithValue(t, "1 error(s) decoding:\n\n* '' has invalid keys: appa", f)
}

func TestNewConfigNotFound(t *testing.T) {
	f := func() {
		New("", "")
	}
	assert.Panics(t, f)
	assert.PanicsWithValue(t, "Config File \"app.config.test\" Not Found in \"[]\"", f)
}

func TestInitViperConfigNotFound(t *testing.T) {
	f := func() {
		initViper("app.config.test")
	}
	assert.Panics(t, f)
	assert.PanicsWithValue(t, "Config File \"app.config.test\" Not Found in \"[]\"", f)
}
